shinyUI(
    fluidPage(
        theme = shinytheme("flatly"),
        tags$head(
            tags$link(rel = "stylesheet", type = "text/css", href = "myCss.css")
        ),
        titlePanel("Stations météo Toulouse Août 2019"),
        hr(),
        br(),
        sliderInput("dates",
                    label="Periode",
                    min = rangeDate[1],
                    max = rangeDate[2],
                    value = c(as.POSIXct("2019-08-09 00:00:00"),
                              as.POSIXct("2019-08-10 00:00:00")),
                    width = '100%'
                    ),
        br(),
        navlistPanel(
            "Visualisations",
            tabPanel("Météo toutes stations",
                     h4("Températures observées"),
                     verbatimTextOutput("summaryTemp"),
                     plotOutput("temperaturePlot"),
                     h4("Ecarts de température"),
                     plotOutput("ecartPlot"),
                     h4("Différence à la médiane des stations"),
                     plotOutput("diffMedianPlot")),
            tabPanel("Analyse par station",
                     fluidRow(
                         column(6,
                                selectInput("station", "Choix de la station",
                                            choices = nomsStations, multiple=TRUE,
                                            selected = nomsStations[1]),
                                selectInput("varStation", "Quelle variable",
                                            choices = c("Temperature", "Humidite", "Pluie"), 
                                            multiple=TRUE,
                                            selected = "Temperature")),
                         column(6,
                                checkboxInput("others", "Afficher les autres stations", value=TRUE)
                         )
                     ),
                     h4("Température observées"),
                     plotOutput("temperatureStation"),
                     h4("Distribution de la température par station"),
                     plotOutput("boxplotStation")),
            tabPanel("Carte des station",
                     leafletOutput("carteStations")),
            "Tableaux",
            tabPanel("Données complètes",
                     DT::dataTableOutput("donneesBrutes")),
            tabPanel("Résumé données par station",
                     DT::dataTableOutput("donneesStations"))
        ),
        br(),
        br(),
        tags$footer(title="Your footer here", 
                    "Source des données", tags$a(href="https://data.toulouse-metropole.fr", "https://data.toulouse-metropole.fr"),
                    align = "left", style = "
                    position:fixed;
                    bottom:0;
                    width:100%;
                    padding: 10px;
                    z-index: 1000;"
                    )
        )
)




