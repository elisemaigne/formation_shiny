# app5 = correction de l'exercice 4
# Exercice 5 à faire à partir du code ci-dessous :
# Et ce ui bien fainéant...
# Utiliser une commande `update` pour charger automatiquement les noms de colonnes dans le selectInput pour choisir la variable. 
# ?shiny::updateSelectInput
# Et une fois que c'est fait, faire la même chose pour le sliderInput. 
# Pour éviter l'erreur au démarrage,  on peut mettre des shiny::validate au début des fonctions render.

# Define UI for application that draws a histogram
shinyUI(fluidPage(

    # Application title
    titlePanel("Mon application avec mes données"),

    # Sidebar with a slider input for number of bins
    sidebarLayout(
        sidebarPanel(
            h4("Paramètres généraux"),
            hr(),
            sliderInput(
                inputId = "myYears",
                label = "Choix des années",
                min = 2011,
                max = 2017,
                value=2011,
                step=1,
                animate = TRUE
            )
        ),

        # Show a plot of the generated distribution
        mainPanel(
            tabsetPanel(
                tabPanel("Histogram", 
                         br(),
                         h4("Paramètres du graphique"),
                         hr(),
                         sliderInput("bins",
                                     "Number of bins:",
                                     min = 1,
                                     max = 50,
                                     value = 30),
                         selectInput(
                             inputId = "myVar",
                             label = "Variable",
                             choices = c("Nb_Programmes", "Nb_Annules", "Nb_Retard")
                         ),
                         textInput(
                             inputId = "myTitle",
                             label = "Titre du graphique",
                             value = "Histogramme"
                         ),
                         plotOutput("distPlot")
                ),
                tabPanel("Data", 
                         br(),
                         tableOutput("myTable")
                )
            )
        )
    )
))
