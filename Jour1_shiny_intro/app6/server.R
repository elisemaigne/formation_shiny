# app6 = correction de l'exercice 5
# Exercice 6 à faire à partir du code ci-dessous :
# On va mettre en ligne l'application, sur https://www.shinyapps.io/. 
# Une bonne application contient tout dans le même dossier :
# monapplication/
#     monapplication.Rproj
# ui.R
# server.R
# data/
#     www/
#     R/ ou modules/
# Copier les données datatrains.csv dans le dossier de l'application (ou dans un sous répertoire data/).
# Changer le chemin du read.csv. 
# Vérifier que l'application marche en local
# Je me connecte à mon compte (ou je le crée) sur https://www.shinyapps.io/.
# Suivre les instructions sur la page : https://www.shinyapps.io/admin/#/dashboard (1 et 2 à ne faire que la première fois).
# Puis rsconnect::deployApp('app6')

# Define server logic required to draw a histogram
shinyServer(function(input, output, session) {
    
    #### Update des inputs au démarrage ####
    observe({
        # sélection variables numériques pour alimenter myVar
        varnum <- colnames(datatrains)[which(sapply(datatrains, is.numeric))]
        if("Annee" %in% varnum){
            varnum <- varnum[-which(varnum == "Annee")]
        }
        updateSelectInput(session, "myVar", choices = varnum)
        
        # sélection des années pour alimenter myYears
        minYear <- min(datatrains$Annee, na.rm = T)
        maxYear <- max(datatrains$Annee, na.rm = T) 
        updateSliderInput(session, 
                          "myYears", 
                          min = minYear,
                          max = maxYear,
                          value = minYear)
    })
    
    #### Réduction du jeu de données selon l'année ####
    dataReduced <- reactive({
        datatrains[datatrains$Annee == input$myYears, ]
    })
    
    #### Graphique ####
    output$distPlot <- renderPlot({
        shiny::validate(
            shiny::need(input$myVar %in% colnames(datatrains), 'Patientez...'),
            shiny::need(input$myYears != 0, 'Patientez...')
        )
        # generate bins based on input$bins from ui.R
        x    <- dataReduced()[, input$myVar]
        bins <- seq(min(x), max(x), length.out = input$bins + 1)
        
        # draw the histogram with the specified number of bins
        hist(x, breaks = bins, 
             col = 'darkgray', 
             border = 'white',
             xlab=input$myVar, 
             main=input$myTitle)
    })
    
    #### Données ####
    output$myTable <- renderTable({
        shiny::validate(
            shiny::need(input$myYears != 0, 'Patientez...')
        )
        dataReduced()
    })
    
})
