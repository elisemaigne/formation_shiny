# app1 = correction de l'exercice 0
# Exercice 1 à faire à partir du code ci-dessous :
# Utiliser les données `datasncf_prepare.csv` dans l'application.
# Exemple de noms de variables numériques du jeu de données :
# "Annee", "Nb_Programmes", "Nb_Annules", "Nb_Retard", "Tx_Regularite", "Tx_Circulation"

library(shiny)

# Define UI for application that draws a histogram
shinyUI(fluidPage(

    # Application title
    titlePanel("Mon nouveau titre"),

    # Sidebar with a slider input for number of bins
    sidebarLayout(
        sidebarPanel(
            sliderInput("bins",
                        "Number of bins:",
                        min = 1,
                        max = 50,
                        value = 30),
            selectInput(
                inputId = "myVar",
                label = "Variable",
                choices = c("eruptions", "waiting")
            )
        ),

        # Show a plot of the generated distribution
        mainPanel(
            plotOutput("distPlot"),
            "Données brutes :",
            tableOutput("myTable")
        )
    )
))
