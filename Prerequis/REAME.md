Merci de vous êtes inscrits à la formation Shiny.

Les horaires seront 9h-17h. 

Quelques infos pratiques avant le jour J.

Il y aura des ordinateurs à disposition dans la salle, ils seront déjà configurés pour la formation. 
Si vous voulez venir avec votre ordinateur perso c'est possible mais voici la liste des pré-requis dans ce cas :

- R >=4.0.1 sur votre machine
- Rstudio
- Les packages suivants installés :
    - shiny
    - rsconnect
    - DT
    - shinydashboard
    - htmltools
    - shinyWidgets
    - shinyjs
    - shinycssloaders
    - shinyjqui
    - ggplot2
    - plotly
    - leaflet
    - shinythemes 

La commande suivante doit vous renvoyer TRUE

```
all(sapply(c("shiny", "rsconnect", "DT", "shinydashboard", "htmltools", "shinyWidgets", "shinyjs", 
         "shinycssloaders", "shinyjqui", "ggplot2", "plotly", "leaflet", "shinythemes"), require, character.only=TRUE))
```

Vous pouvez aussi lancer un exemple de shiny avec la commande suivante qui doit vous lancer une application :

`shiny::runExample("01_hello")`

Si ça ne marche pas merci de nous contacter en amont de la formation.


Le second jour de formation vous développerez une application de A à Z. 

Si vous souhaitez prendre des données avec lesquelles vous êtes familier.es et un bout de code R qui produit une analyse (dans l'idéal simple et relativement rapide, par exemple un ou plusieurs graphique) à partir de ces données vous pouvez les apporter avec vous, vous pourrez développer une application à partir de ça. 

Sinon on vous fournira des données et des idées d'application dont vous pourrez vous inspirer pour créer la votre. 


Elise Maigné & Nathalie Vialaneix
elise.maigne@inrae.fr
nathalie.vialaneix@inrae.fr
